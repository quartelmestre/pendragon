// Main system variable

export const PENDRAGON = {};

export function localizeAbilities() {
    const res = {
      general: {
        label: game.i18n.localize(PENDRAGON.ABILITIES_CATEGORIES.general.mnemonic),
        mnemonic: PENDRAGON.ABILITIES_CATEGORIES.general.mnemonic,
        option: false,
        selection: "disabled"
      },
      ...translateAndSort(PENDRAGON.GENERAL_ABILITIES),
      academic: {
        label: game.i18n.localize(PENDRAGON.ABILITIES_CATEGORIES.academic.mnemonic),
        mnemonic: PENDRAGON.ABILITIES_CATEGORIES.academic.mnemonic,
        option: false,
        selection: "disabled"
      },
      ...translateAndSort(PENDRAGON.ACADEMIC_ABILITIES),
      arcane: {
        label: game.i18n.localize(PENDRAGON.ABILITIES_CATEGORIES.arcane.mnemonic),
        mnemonic: PENDRAGON.ABILITIES_CATEGORIES.arcane.mnemonic,
        option: false,
        selection: "disabled"
      },
      ...translateAndSort(PENDRAGON.ARCANE_ABILITIES),
      martial: {
        label: game.i18n.localize(PENDRAGON.ABILITIES_CATEGORIES.martial.mnemonic),
        mnemonic: PENDRAGON.ABILITIES_CATEGORIES.martial.mnemonic,
        option: false,
        selection: "disabled"
      },
      ...translateAndSort(PENDRAGON.MARTIAL_ABILITIES),
      supernaturalCat: {
        label: game.i18n.localize(PENDRAGON.ABILITIES_CATEGORIES.supernaturalCat.mnemonic),
        mnemonic: PENDRAGON.ABILITIES_CATEGORIES.supernaturalCat.mnemonic,
        option: false,
        selection: "disabled"
      },
      ...translateAndSort(PENDRAGON.SUPERNATURAL_ABILITIES),
      mystery: {
        label: game.i18n.localize(PENDRAGON.ABILITIES_CATEGORIES.mystery.mnemonic),
        mnemonic: PENDRAGON.ABILITIES_CATEGORIES.mystery.mnemonic,
        option: false,
        selection: "disabled"
      },
      ...translateAndSort(PENDRAGON.MYSTERY_ABILITIES)
    };
    return res;
  }

  function translateAndSort(abilityList) {
    for (let [key, value] of Object.entries(abilityList)) {
      let translation;
      if (value.option)
        translation = game.i18n.format(value.mnemonic, {
          option: game.i18n.localize(value.optionPlaceholder)
        });
      else translation = game.i18n.localize(value.mnemonic);
      abilityList[key].label = translation;
    }
    let tmp = Object.entries(abilityList).sort((a, b) => {
      return a[1].label.localeCompare(b[1].label);
    });
    return Object.fromEntries(tmp);
    // let tmp = abilityList.map(a => {
    //   let translation;
    //   if (a.option)
    //     translation = game.i18n.format(a[1].mnemonic, {
    //       option: game.i18n.localize(a[1].optionPlaceholder)
    //     });
    //   else translation = game.i18n.localize(a[1].mnemonic);
    //   return { [a[0]]: { name: translation, ...a[1] } };
    // });
    // let tmp2 = Object.values(tmp);
    // return tmp2.sort((a, b) => a.name.localeCompare(b.name));
  }
  export function localizeCategories() {
    let result = {};
    for (let [key, value] of Object.entries(PENDRAGON.ABILITIES_CATEGORIES)) {
      result[key] = { mnemonic: value.mnemonic, label: game.i18n.localize(value.mnemonic) };
    }
    return result;
  }
  