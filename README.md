# Pendragon for Foundry VTT

![GitLab Release](https://img.shields.io/gitlab/v/release/53340120?label=published%20release&color=%230033cc)
![Dynamic JSON Badge](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fquartelmestre%2Fpendragon%2F-%2Fraw%2Fmain%2Fsystem.json&query=%24.version&label=current%20version&color=%23336600&prefix=v)
![Dynamic JSON Badge](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fquartelmestre%2Fpendragon%2F-%2Fraw%2Fmain%2Fsystem.json&query=%24.compatibility.verified&prefix=v&label=Foundry%20verified)

This project is an implementation of [Pendragon v5](https://www.chaosium.com/pendragon/) rules for [Foundry VTT](https://foundryvtt.com/).

## Installation

Download the [latest release](https://gitlab.com/quartelmestre/pendragon/-/releases) zip asset and unzip it as a folder under Foundry's `Data/systems/` folder.
